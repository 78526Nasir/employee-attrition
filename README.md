### KEY FACTORS OF EMPLOYEE ATTRITION: A COMPARISONS BETWEEN SUPERVISED FEATURE SELECTION AND UNSUPERVISED DIMENSIONALITY REDUCTION TECHNIQUE.

> A thesis submitted in partial fulfillment of the requirement for the degree of Bachelor of Science in Software Engineering.

---
** The Novelty of this thesis: ** 

* Explore the true **reasons** behind **Employee Attrition**.
* Visualize all the key factors using `bar`, `swarm`, `count`, `facet grid`, `box` plots.
* Total **8** different Data-preprocessing techniques were used in order to get the optimum, clean and tidy dataset.
    * Label Encoding 
    * Removing Identical and Constant Features
    * Pearson's Correlation Coefficient
    * Standard Scalar
    * Tukey boxplot
    * Synthetic Minority Over-sampling Technique (SMOTE)
    * Permutation Importance (PIMP)
    * Principle Component Analysis (PCA)
* Perform a **Comparisons Analysis** between Supervised Feature Selection Technique: **Permutation Importance** and Unsupervised Dimensionality Reduction Technique: **PCA**.
* Proposed a Novel model that **outperforms** state-of-the-art models. 
* Total **9** Different Evaluation Metrics used in order to **evaluate** the models.
    * Accuracy Score
    * Precision Score
    * Sensitivity
    * Specificity
    * Area Under the Curve (AUC)
    * Receiver Operating Characteristics (ROC)
    * F1 Score
    * Matthews Correlation Coefficient
    * Calibration Curve